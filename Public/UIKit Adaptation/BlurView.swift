//
//  BlurView.swift
//  Teacher
//
//  Created by qh w on 2021/5/4.
//

import SwiftUI

struct BlurView: UIViewRepresentable {
    func makeUIView(context: Context) -> UIVisualEffectView {
        let effect = UIBlurEffect(style: .dark)
        let view = UIVisualEffectView(effect: effect)
        return view
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        return
    }
}

struct BlurView_Previews: PreviewProvider {
    static var previews: some View {
        BlurView().edgesIgnoringSafeArea(.all)
    }
}
