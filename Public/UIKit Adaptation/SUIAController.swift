//
//  TextFieldAlert.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

struct SUIAController: UIViewControllerRepresentable {
    @Binding var showAlert: Bool
    @Binding var alert: UIAlertController
    init(showAlert: Binding<Bool>, _ alertController: Binding<UIAlertController>) {
        self._showAlert = showAlert
        self._alert = alertController
    }
    
    func makeUIViewController(context: Context) -> UIViewController {
        return UIViewController()
    }
    
    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        if self.showAlert {
            DispatchQueue.main.async {
                self.showAlert = false
            }
            if uiViewController.isBeingPresented || uiViewController.presentedViewController != nil {
                return
            }
            uiViewController.present(alert, animated: true, completion: nil)
        }
    }
}
