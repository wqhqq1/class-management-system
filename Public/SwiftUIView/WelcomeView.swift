//
//  WelcomView.swift
//  ClassManager
//
//  Created by qh w on 2021/3/6.
//

import SwiftUI
import CarBode
import AVFoundation

struct WelcomeView<Content: View>: View {
    @State var isChoosable = false
    @State var load = false
    @State var isAbleToLoad = false
    @State var loadCard = [false, false, false]
    @State var hasGoneSetting = false
    @State var selection: Bool = false
    @State var requestCamera: Bool = false
    @ViewBuilder var content: () -> Content
    var body: some View {
        DispatchQueue.main.async {
            _ = CoreData(classNameStr: "").downloadDataByHttp()
            self.requestCamera = false
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            withAnimation(.easeOut(duration: 0.5)) {
                loadCard[0] = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                    withAnimation(.easeOut(duration: 0.5)) {
                        loadCard[1] = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                            withAnimation(.easeOut(duration: 0.5)) {
                                loadCard[2] = true
                                isAbleToLoad = true
                            }
                        }
                    }
                }
            }
        }
        return
            ZStack {
                if requestCamera {
                    CBScanner(supportBarcode: .constant([.qr]), onFound: {_ in})
                }
                if !load {
                    VStack(alignment: .leading) {
                        Image("launcher").cornerRadius(5)
                        Text("课程管理系统")
                            .font(.largeTitle)
                            .bold()
                        HStack {
                            Text("2023 Autumn")
                                .font(.title)
                            Divider()
                                .frame(height: 30)
                            Text("Beta")
                                .font(.title)
                                .foregroundColor(.green)
                        }
                        if loadCard[0] {
                            HStack {
                                Image(systemName: "studentdesk")
                                    .resizable()
                                    .scaledToFit()
                                    .foregroundColor(.green)
                                    .frame(width: 60, height: 60)
                                Text("通过简单易操作的管理系统设置课程")
                                    .font(.title)
                            }.transition(AnyTransition.opacity.combined(with: .offset(x: 0, y: 20)))
                        }
                        if loadCard[1] {
                            HStack {
                                Image(systemName: "signature")
                                    .resizable()
                                    .scaledToFit()
                                    .foregroundColor(.blue)
                                    .frame(width: 60, height: 60)
                                Text("自动提醒教师签到，统计出勤记录")
                                    .font(.title)
                            }.transition(AnyTransition.opacity.combined(with: .offset(x: 0, y: 20)))
                        }
                        if loadCard[2] {
                            HStack {
                                Image(systemName: "rectangle.and.pencil.and.ellipsis")
                                    .resizable()
                                    .scaledToFit()
                                    .foregroundColor(.orange)
                                    .frame(width: 60, height: 60)
                                Text("发送反馈：")
                                    .font(.title)
                                Link("点击此处", destination:
                                        URL(string: "https://gitee.com/wqhqq1/class-management-system")!)
                            }.transition(AnyTransition.opacity.combined(with: .offset(x: 0, y: 20)))
                        }
                        Spacer()
                    }
                    .padding(.top, 100)
                    VStack {
                        Spacer()
                        HStack {
                            Spacer()
                            ZStack {
                                RoundedRectangle(cornerRadius: 10)
                                    .foregroundColor(isAbleToLoad ? .blue:.init("disabled"))
                                    .frame(width: 200, height: 40)
                                if !isAbleToLoad {
                                    ProgressView()
                                        .progressViewStyle(CircularProgressViewStyle())
                                }
                                else {
                                    Text(hasGoneSetting ? "继续":"前往\"设置\"")
                                        .foregroundColor(.white)
                                        .font(.headline)
                                        .sheet(isPresented: self.$selection) {
                                            NavigationView {
                                                SettingsView(showCloseButton: true)
                                            }
                                        }
                                }
                            }
                            .padding(.bottom)
                            .onTapGesture {
                                if !hasGoneSetting {
                                    self.selection = true
                                    hasGoneSetting = true
                                    return
                                }
                                if isAbleToLoad {
                                    withAnimation(.default) {
                                        load = true
                                        UserDefaults.standard.set(true, forKey: "isNotFirstLaunch")
                                    }
                                }
                            }
                            Spacer()
                        }
                    }
                }
                if load {
                    LoadingView(content: content, classChoosable: self.isChoosable)
                }
            }
    }
}
