//
//  IsUploadingWarning.swift
//  ClassManager
//
//  Created by qh w on 2021/5/16.
//

import SwiftUI

struct IsUploadingWarning: View {
    @Binding var showWarning: Bool
    @State var realShow = false
    var body: some View {
        DispatchQueue.main.async {
            if showWarning {
                withAnimation(.spring()) {
                    self.realShow = true
                }
            }
            else {
                withAnimation(.default) {
                    self.realShow = false
                }
            }
        }
        return ZStack {
            Rectangle()
                .frame(width: 200, height: 80)
                .cornerRadius(40)
                .shadow(radius: 4)
                .foregroundColor(.white)
            HStack {
                Circle()
                    .overlay(Image(systemName: "exclamationmark.triangle.fill")
                                .imageScale(.large)
                                .foregroundColor(.white))
                    .frame(width: 50, height: 50)
                    .foregroundColor(.blue)
                Text("正在同步...\n不要关闭App")
            }
        }
        .offset(y: !realShow ? 150:-10)
    }
}

struct IsUploadingWarning_test: View {
    @State var show = false
    var body: some View {
        ZStack {
            Button(action: {
                withAnimation(.spring()) {
                    show.toggle()
                }
            }) {
                Text("开关")
            }
            VStack {
                Spacer()
                IsUploadingWarning(showWarning: self.$show)
            }
        }
    }
}
struct IsUploadingWarning_Preview: PreviewProvider {
    static var previews: some View {
        IsUploadingWarning_test()
    }
}
