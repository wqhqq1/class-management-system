//
//  RoundedProgressStyle.swift
//  ClassManager
//
//  Created by wqh on 1/29/21.
//

import SwiftUI

struct RoundedProgressViewStyle<Content: View>: ProgressViewStyle {
    var overlay: () -> Content
    init(@ViewBuilder overlay: @escaping () -> Content) {
        self.overlay = overlay
    }
    func makeBody(configuration: LinearProgressViewStyle.Configuration) -> some View {
        return VStack {
            ZStack {
                Circle()
                    .stroke(lineWidth: 15.0)
                    .opacity(0.3)
                    .foregroundColor(Color.accentColor.opacity(0.5))
                
                Circle()
                    .trim(from: CGFloat(configuration.fractionCompleted ?? 0) > 0.5 ? 0.5:1-CGFloat(configuration.fractionCompleted ?? 0)
                          , to: 1)
                    .stroke(style: StrokeStyle(lineWidth: 15.0, lineCap: .round, lineJoin: .round))
                    .rotationEffect(Angle(degrees: 90))
                    .rotation3DEffect(Angle(degrees: 180), axis: (x: 1, y: 0.0, z: 0.0))
                    .foregroundColor(Color.accentColor)
                if configuration.fractionCompleted ?? 0 > 0.5 {
                    Circle()
                        .trim(from: 1-CGFloat(configuration.fractionCompleted ?? 0)+0.5, to: 1)
                        .stroke(style: StrokeStyle(lineWidth: 15.0, lineCap: .round, lineJoin: .round))
                        .rotationEffect(Angle(degrees: 90))
                        .rotation3DEffect(Angle(degrees: 180), axis: (x: 0, y: 1.0, z: 0.0))
                        .foregroundColor(Color.accentColor)
                }
                VStack {
                    overlay()
                }
            }
        }
    }
}
