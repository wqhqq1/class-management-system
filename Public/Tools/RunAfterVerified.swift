//
//  RunAfterVerified.swift
//  ClassManager
//
//  Created by wqh on 1/28/21.
//

import LocalAuthentication

func RunAfterVierified(afterFailed: @escaping () -> Void = {}, skip: Bool = false, Codes: @escaping () -> Void) {
    if skip {
        DispatchQueue.main.async {
            Codes()
        }
        return
    }
    let authenticationContext = LAContext()
    authenticationContext.evaluatePolicy(.deviceOwnerAuthentication
                                         , localizedReason: "完成此操作需要验证管理权限") { (success, error) in
        if success {
            DispatchQueue.main.async {
                Codes()
            }
        }
        if let _ = error {
            DispatchQueue.main.async {
//                // print(error)
                afterFailed()
            }
        }
    }
}
