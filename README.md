# 2023 Autumn 开发计划
- [x] 动画细节优化
- [x] 性能优化
- [x] BLE打卡
- [x] BLE打卡相关错误受理（超时、权限等）
- [x] 用户界面展示支持BLE的班级
- [x] 小组件支持打卡（iOS 17）
- [ ] WatchOS app

# 预览图
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/01.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/02.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/03.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/04.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/05.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/06.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/07.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/08.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/09.jpeg)
![avatar](https://gitee.com/wqhqq1/class-management-system/raw/master/Previews/10.jpeg)
