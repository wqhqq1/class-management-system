//
//  File.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

struct CalenderEditingView: View {
    @EnvironmentObject var UserData: CoreData
    @State var isAllowed = true
    @State var once = false
    @State var ShowAdd = false
    @State var showAlert = false
    @State var ShowSheet = [Bool](repeating: false, count: 1000)
    @State var isChanging = true
    @Binding var allowUpdate: Bool
    @State var UIAController = UIAlertController()
    @State var selected: Int = -1
    //    @Binding var selection: Int?
    //    @Binding var isVerified: Bool
    var body: some View {
        //        // print(UserData.events)
        DispatchQueue.main.async {
            var flag = false
            for data in ShowSheet {
                if data {
                    flag = true
                }
            }
            if ShowAdd {
                flag = true
            }
            if flag {
                allowUpdate = false
            } else {
                allowUpdate = true
            }
        }
        return !self.isAllowed ? nil:ScrollView {
            ForEach(UserData.events) { _event in
                if !_event.isRemoved {
                    Fluent_Rect {
                        VStack(alignment: .leading) {
                            HStack {
                                Text(UserData.classes[_event._class].name)
                                    .font(.title)
                                Spacer()
                                Text("任课老师:\(_event.techer)")
                                    .font(.title)
                            }
                            Spacer()
                            HStack {
                                Text("始末时间:\(_event.start.string()) ~ \(_event.end.string())")
                                Spacer()
                            }
                            HStack {
                                Text("持续时间:\(Int((_event.end-_event.start)/60))分钟")
                                Spacer()
                                Text("状态:\(_event.state.convertToString())")
                            }
                            if selected == _event.id {
                                Divider()
                                Button(action: {
                                    UserData.removeevent(at: [_event.id])
                                }) {
                                    Image(systemName: "trash.fill")
                                        .foregroundColor(.red)
                                        .imageScale(.large)
                                }
                                Text("提示：长按可以编辑课程")
                                    .foregroundColor(.gray)
                            }
                        }
                        .foregroundColor(.white)
                        .padding(.all)
                    }
                    .onTapGesture {
                        withAnimation(.spring()) {
                            if selected == _event.id {
                                selected = -1
                            }
                            else {
                                selected = _event.id
                            }
                        }
                    }
                    .frame(height: selected == _event.id ? 250:170)
                    .contextMenu(ContextMenu(menuItems: {
                        Button(action: {
                            isChanging = true
                            self.ShowSheet[_event.id] = true
                        }) {
                            Text("调课")
                            Image(systemName: "repeat")
                        }
                        Button(action: {
                            isChanging = false
                            self.ShowSheet[_event.id] = true
                        }) {
                            Text("修改")
                            Image(systemName: "square.and.pencil")
                        }
                    }))
                    .transition(.slide)
                    .sheet(isPresented: $ShowSheet[_event.id], content: {
                        AddeventView(isChanging: self.isChanging, index: _event.id, teacher: _event.techer, _class: _event._class, _repeat: _event._repeat, start: Date(timeIntervalSince1970: _event.start),
                                      end: (_event.end - _event.start)/60, state: _event.state)
                            .environmentObject(UserData)
                    })
                }
                
            }
        }
        .sheet(isPresented: self.$ShowAdd) {
            AddeventView().environmentObject(UserData)
        }
        .background(SUIAController(showAlert: $showAlert, $UIAController))
        .navigationBarItems(trailing: Button(action: {
            if UserData.classes.count != 0 {
                self.ShowAdd = true
            }
            else {
                UIAController = UIAlertController(title: "致命错误", message: "添加完课程类型后方可添加课程", preferredStyle: .alert)
                UIAController.addAction(UIAlertAction(title: "确定", style: .default))
                self.showAlert = true
            }
        }) {
            Image(systemName: "plus")
            
        })
        .navigationBarTitle("添加/修改课程表")
    }
}


