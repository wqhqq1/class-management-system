//
//  ClassSetting.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

let BookColors: [Color] = [.blue, .green, .yellow, .orange, .pink, .purple] //Define some color to use in order
var index_for_delete: IndexSet? = nil

struct ClassSettingView: View {
    @EnvironmentObject var UserData: CoreData
    @State var showAddAlert = false
    @State var addedText = ""
    @State var isAllowed = true
    @State var once = false
    //    @Binding var selection: Int?
    @State var isGoingTo = false
    @State var isOpened = false
    @State var Offsets = [CGFloat](repeating: 0, count: 2000)
    @State var isTooMuch = false
    @State var AlertController = UIAlertController()
    //    @Binding var isVerified: Bool
    @State var selected = -1
    @Environment(\.colorScheme) var _Color: ColorScheme
    var body: some View {
        return !self.isAllowed ? nil:ScrollView {
            VStack {
                ForEach(UserData.classes) { __class in
                    Fluent_Rect {
                        VStack(alignment: .leading) {
                            HStack {
                                Image(systemName: "a.book.closed.fill")
                                    .resizable()
                                    .frame(width: 40, height: 40)
                                    .fixedSize()
                                Text(__class.name)
                                    .font(.headline)
                                Spacer()
                            }
                            .foregroundColor(.white)
                            if selected == __class.id {
                                Divider()
                                Button(action: {
                                    index_for_delete = [__class.id]
                                    AlertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                                    AlertController.title = "确认删除？"
                                    AlertController.message = "删除课程类型将会一并删除相关所有课程，您确定继续吗？"
                                    AlertController.addAction(UIAlertAction(title: "取消", style: .cancel))
                                    AlertController.addAction(UIAlertAction(title: "仍然继续", style: .destructive, handler: { _ in
                                        UserData.removeClass_confirmed(at: index_for_delete ?? [])
                                    }))
                                    self.showAddAlert = true
                                    withAnimation(.spring()) {
                                        Offsets[__class.id] = 0
                                    }
                                }) {
                                    Image(systemName: "trash.fill")
                                        .imageScale(.large)
                                        .foregroundColor(.red)
                                }
                            }
                        }
                        .padding(.horizontal)
                    }
                    .onTapGesture {
                        withAnimation(.spring()) {
                            if __class.id == selected {
                                selected = -1
                            }
                            else {
                                self.selected = __class.id
                            }
                        }
                    }
                    .frame(height: __class.id == selected ?  130:100)
                    .transition(.slide)
                    Spacer()
                }
                .background(SUIAController(showAlert: self.$showAddAlert, self.$AlertController).environmentObject(UserData))
                .navigationBarItems(trailing:
                                        Button(action: {
                    if UserData.classes.count > 1900 {
                        self.isTooMuch = true
                    }
                    else {
                        AlertController =  UIAlertController(title: "添加课程类型", message: "请在下面的文本框输入课程类型名", preferredStyle: .alert)
                        AlertController.addTextField(configurationHandler: { textField in
                            textField.placeholder = "课程类型"
                            textField.clearButtonMode = .whileEditing
                        })
                        AlertController.addAction(UIAlertAction(title: "确定", style: .default, handler: { _ in
                            AlertController.textFields?.forEach({ TF in
                                self.UserData.addClass(name: TF.text ?? "")
                            })
                        }))
                        AlertController.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
                        self.showAddAlert = true
                    }
                }) {
                    Image(systemName: "plus")
                }
                )
                .navigationBarTitle("添加课程类型")
            }
        }
    }
}
