//
//  SchoolApp.swift
//  School
//
//  Created by qh w on 2021/5/3.
//

import SwiftUI

@main
struct SchoolApp: App {
    @State var isNotFirstLaunch = UserDefaults.standard.bool(forKey: "isNotFirstLaunch")
    @State var isVerified = false
    @Environment(\.scenePhase) var scenePhase
    var body: some Scene {
        WindowGroup {
            if isNotFirstLaunch {
                LoadingView(content: {ContentView(UserData: CoreData(classNameStr: ""), showWarning: .constant(false))}, useBiometryProtect: true, classChoosable: true)
            }
            else {
                WelcomeView(isChoosable: true, content: {ContentView(UserData: CoreData(classNameStr: ""), showWarning: .constant(false))}).environmentObject(CoreData(classNameStr: ""))
            }
        }.onChange(of: scenePhase) { phase in
            if phase == .background {
//                exit(0)
                self.isVerified = false
            }
            if phase == .active && !UserDefaults.standard.bool(forKey: "2023Beta3Fix") && UserDefaults.standard.bool(forKey: "isNotFirstLaunch") && UserDefaults(suiteName: "group.classmanager")!.string(forKey: "server") != nil {
                Patch2023Beta3Fix()
                UserDefaults.standard.set(true, forKey: "2023Beta3Fix")
            }
        }
    }
}
