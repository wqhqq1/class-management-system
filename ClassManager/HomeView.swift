//
//  HomeView.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

struct HomeView: View {
    @State var show = [Bool](repeating: false, count: 3)
    @State var now = Date().timeIntervalSince1970
    @State var ShowSheet = [Bool](repeating: false, count: 1000)
    @Binding var isVerified: Bool
    @State var timer = Timer.TimerPublisher(interval: 5, runLoop: .current, mode: .default).autoconnect()
    @EnvironmentObject var UserData: CoreData
    var body: some View {
        ZStack {
            ForEach(UserData.events) { _event in
                if !_event.isRemoved {
                    Rectangle()
                        .opacity(0.001)
                        .sheet(isPresented: $ShowSheet[_event.id]) {
                            SignInView(Index: _event.id)
                                .environmentObject(UserData)
                        }
                        .onReceive(timer) { _ in
                            withAnimation(.default) {
                                now = Date().timeIntervalSince1970
                                if (_event.start - now)/60 <= 0 && (_event.end - now)/60 >= 0 &&
                                    (_event.state == 0 || _event.state == 2) {
                                    show[0] = false;show[2] = false
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                        show[1] = true
                                    }
                                }
                                if (_event.start - now)/60 <= 0 && _event.state == -1 && _event.end - now > 0 && show[1] == false {
                                    ShowSheet[_event.id] = true
                                }
                                if Int((_event.start - now)/60) < -5 && UserData.events[_event.id].state == -1 {
                                    UserData.changeeventState(at: _event.id, to: .failed)
                                }
                                if _event._repeat && _event.end - now < 0{
                                    UserData.repeateventNextWeek(at: _event.id)
                                }
                            }
                        }
                }
            }
            VStack {
                HStack {
                    AttendanceView(show: self.$show)
                        .environmentObject(UserData)
                        .padding([.top, .leading, .trailing], 5.0)
                        .onTapGesture {
                            ToggleShows(0)
                        }
                    TimerView(show: self.$show)
                        .environmentObject(UserData)
                        .padding([.top, .trailing], 5.0)
                }
                ClassesView(show: self.$show, isVerified: self.$isVerified, ShowSheet: self.$ShowSheet)
                    .environmentObject(UserData)
                    .padding(5)
                    .onTapGesture {
                        ToggleShows(2)
                    }
            }
            .navigationBarTitle("课程一览")
        }
    }
    private func ToggleShows(_ index: Int) {
        for idx in 0..<show.count {
            show[idx] = (idx == index) ? true:false
        }
    }
}
