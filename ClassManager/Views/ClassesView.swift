//
//  ClassesView.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

struct ClassesView: View {
    @EnvironmentObject var UserData: CoreData
    @Environment(\.colorScheme) var _Color: ColorScheme
    @Binding var show: [Bool]
    @Binding var isVerified: Bool
    @State var once = false
    @State var width: CGFloat? = nil
    @State var height: CGFloat? = nil
    @Binding var ShowSheet: [Bool]
    @State var ShowEdit = [Bool](repeating: false, count: 1000)
    @State var now: TimeInterval = Date().timeIntervalSince1970
    @State var timer = Timer.TimerPublisher(interval: 5, runLoop: .current, mode: .default).autoconnect()
    @State var offset = [CGFloat](repeating: 0, count: 1000)
    @State var isGoingTo = [Bool](repeating: false, count: 1000)
    @State var isLeft = [Bool](repeating: false, count: 1000)
    @State var showSheet = [Bool](repeating: false, count: 1000)
    var body: some View {
        if show[0] || show[1] {
            DispatchQueue.main.async {
                withAnimation(.spring()) {
                    height = 0
                }
            }
        }
        if !show[0] && !show[1] && !show[2] {
            DispatchQueue.main.async {
                withAnimation(.spring()) {
                    height = nil
                }
            }
        }
        return Rectangle()
            .cornerRadius(15)
            .shadow(radius: 30, x: 5, y: 5)
            .foregroundColor(_Color == .dark ? Color.white.opacity(0.2):.white)
            .frame(width: width, height: height)
            .overlay(
                VStack{
                    if width != 0 && height != 0 {
                        HStack {
                            Text("接下来...")
                                .font(.title)
                                .fontWeight(.semibold)
                            Spacer()
                            if show[2] {
                                Image(systemName: "xmark.circle.fill")
                                    .imageScale(.large)
                                    .foregroundColor(.gray)
                                    .onTapGesture {
                                        show[0] = false;show[1] = false;show[2] = false
                                    }
                            }
                        }
                        ZStack {
                            VStack(spacing: 0) {
                                ScrollView {
                                    VStack {
                                        ForEach(UserData.events) { _event in
                                            if Int((_event.start - now)/60) >= -5 && _event.state != 0
                                                && (isToday(date: _event.start) || show[2]) && !_event.isRemoved {
                                                Fluent_Rect {
                                                    HStack {
                                                        Text(UserData.classes[_event._class].name)
                                                            .font(.title)
                                                        Spacer()
                                                        Text("任课老师:\(_event.techer)")
                                                            .font(.headline)
                                                        Spacer()
                                                        if (_event.start - now)/60 >= 1 {
                                                            if isToday(date: _event.start) {
                                                                Text("离上课还有:\(Int((_event.start - now)/60))分钟")
                                                            }
                                                            else {
                                                                Text("离上课还有:\(Int((_event.start - now)/DAY))天")
                                                            }
                                                        } else if (_event.start - now)/60 <= 1 && (_event.start - now)/60 >= 0 {
                                                            Text("离上课还有不到一分钟")
                                                        }
                                                        else {Text("上课时间已到")}
                                                        if _event.state != -1 && show[2] {
                                                            Image(systemName: State[_event.state])
                                                                .foregroundColor(StateColor[_event.state])
                                                                .padding(.leading)
                                                        }
                                                        if -5 <= Int((_event.start - now)/60) && Int((_event.start - now)/60) <= 5
                                                                && _event.state != 0 {
                                                            Fluent_Rect(ForeGroundColor: .green) {
                                                                Button(action: {
                                                                    showSheet[_event.id] = true
                                                                }) {
                                                                    Image(systemName: "rectangle.and.pencil.and.ellipsis")
                                                                        .imageScale(.large)
                                                                        .foregroundColor(.white)
                                                                }
                                                            }
                                                            .frame(width: 100, height: 90)
                                                        }
                                                    }
                                                    .foregroundColor(.white)
                                                    .onReceive(timer, perform: { _ in
                                                        now = Date().timeIntervalSince1970
                                                    })
                                                    .sheet(isPresented: $showSheet[_event.id]) {
                                                        SignInView(Index: _event.id)
                                                            .environmentObject(UserData)
                                                    }
                                                    .padding(.all)
                                                }
                                                .frame(height: 120)
                                            }
                                        }
                                        .padding(.bottom)
                                    }
                                }
                                
                                Spacer()
                            }
                        }
                        if show[2] {
                            HStack {
                                Text("已完成")
                                    .font(.title)
                                    .fontWeight(.semibold)
                                Spacer()
                            }
                            ZStack {
                                VStack(spacing: 0) {
                                    ScrollView {
                                        VStack {
                                            ForEach(UserData.events) { _event in
                                                if (Int((_event.start - now)/60) < -5 || _event.state == 0) && show[2] && !_event.isRemoved {
                                                    Fluent_Rect {
                                                        HStack {
                                                            Text(UserData.classes[_event._class].name)
                                                                .font(.title)
                                                            Spacer()
                                                            Text("任课老师:\(_event.techer)")
                                                                .font(.headline)
                                                            Spacer()
                                                            Text("上课日期:\(_event.start.string())")
                                                            if UserData.events[_event.id].state != -1 && show[2] {
                                                                Fluent_Rect(ForeGroundColor: StateColor[_event.state]) {
                                                                    Image(systemName: State[_event.state])
                                                                        .foregroundColor(.white)
                                                                        .imageScale(.large)
                                                                }
                                                                .frame(width: 100, height: 90)
                                                            }
                                                        }
                                                        .padding(.horizontal)
                                                        .foregroundColor(.white)
                                                        .sheet(isPresented: $showSheet[_event.id]) {
                                                            SignInView(Index: _event.id, changeTo: .warning)
                                                                .environmentObject(UserData)
                                                        }
                                                    }
                                                    .frame(height: 120)
                                                }
                                            }
                                        }
                                    }
                                    Spacer()
                                }
                            }
                        }
                        Spacer()
                    }
                }.padding()
            )
    }
    let State = ["checkmark", "xmark", "triangle"], StateColor: [Color] = [.green, .red, .orange]
    private func isToday(date: TimeInterval) -> Bool {
        let date = date.string(format: "yyddMMM"), today = Date().timeIntervalSince1970.string(format: "yyddMMM")
        //        // print(date, today)
        if date == today {
            return true
        }
        return false
    }
}
