//
//  AttendanceView.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

struct AttendanceView: View {
    @EnvironmentObject var UserData: CoreData
    @Environment(\.colorScheme) var ColorMode: ColorScheme
    @Binding var show: [Bool]
    @State var width: CGFloat? = nil
    @State var height: CGFloat? = nil
    var body: some View {
        if show[1] {
            DispatchQueue.main.async {
                withAnimation(.spring()) {
                    width = 0
                }
            }
        }
        if show[2] {
            DispatchQueue.main.async {
                withAnimation(.spring()) {
                    height = 0
                }
            }
        }
        if !show[0] && !show[1] && !show[2] {
            DispatchQueue.main.async {
                withAnimation(.spring()) {
                    width = nil;height = nil
                }
            }
        }
        return Rectangle()
            .foregroundColor(ColorMode == .dark ? Color.white.opacity(0.2):.white)
            .cornerRadius(15)
            .shadow(radius: 30, x: 5, y: 5)
            .frame(width: width, height: height)
            .overlay(
                VStack(alignment: .leading) {
                    if width != 0 && height != 0 {
                        VStack {
                            HStack {
                                Text("出勤率")
                                    .font(.title)
                                    .fontWeight(.semibold)
                                Spacer()
                                if show[0] {
                                    Image(systemName: "xmark.circle.fill")
                                        .imageScale(.large)
                                        .foregroundColor(.gray)
                                        .onTapGesture {
                                            show[0] = false;show[1] = false;show[2] = false
                                        }
                                }
                            } //出勤率title
                            ScrollView {
                                VStack {
                                    ForEach(UserData.classes) { __class in
                                        if __class.progress[0] !=  0 || __class.progress[1] != 0 || __class.progress[2] != 0 {
                                            Fluent_Rect {
                                                HStack(alignment: .center) {
                                                    Text(__class.name).font(.title)
                                                    Spacer()
                                                    VStack(alignment: .center) {
                                                        GeometryReader { geo in
                                                            HStack(alignment: .center) {
                                                                if __class.progress[0] != 0 {
                                                                    Rectangle()
                                                                        .foregroundColor(.green)
                                                                        .frame(width: geo.size.width*__class.progress[0], height: 5)
                                                                        .cornerRadius(2.5)
                                                                }
                                                                if __class.progress[2] != 0 {
                                                                    Rectangle()
                                                                        .foregroundColor(.orange)
                                                                        .frame(width: geo.size.width*__class.progress[2], height: 5)
                                                                        .cornerRadius(2.5)
                                                                }
                                                                if __class.progress[1] != 0 {
                                                                    Rectangle()
                                                                        .foregroundColor(.red)
                                                                        .frame(width: geo.size.width*__class.progress[1], height: 5)
                                                                        .cornerRadius(2.5)
                                                                }
                                                            }
                                                        }
                                                        HStack {
                                                            Rectangle()
                                                                .foregroundColor(.green)
                                                                .frame(width: 20, height: 20)
                                                            Text("\(show[0] ? "准时:":"")\(Int(__class.progress[0]*100))%")
                                                            //MARK: 禁用补签功能
//                                                            Spacer()
//                                                            Rectangle()
//                                                                .foregroundColor(.orange)
//                                                                .frame(width: 20, height: 20)
//                                                            Text("\(show[0] ? "迟到:":"")\(Int(__class.progress[2]*100))%")
                                                            Spacer()
                                                            Rectangle()
                                                                .foregroundColor(.red)
                                                                .frame(width: 20, height: 20)
                                                            Text("\(show[0] ? "未到:":"")\(100-Int(__class.progress[0]*100)-Int(__class.progress[2]*100))%")
                                                        }
                                                    }
                                                }
                                                .foregroundColor(.white)
                                                .padding(.all)
                                            }
                                            .frame(height: 100)
                                        }
                                    }
                                }
                            } //出勤率list
                            .listRowBackground(ColorMode == .dark ? Color.white.opacity(0.2):.white)
                            if show[0] {
                                HStack {
                                    Text("排行榜")
                                        .font(.title)
                                        .fontWeight(.semibold)
                                    Spacer()
                                } //排行榜title
                                ScrollView {
                                    VStack {
                                        ForEach(self.UserData.sortedClasses) { __class in
                                            if __class.progress[0] != 0 || __class.progress[1] != 0 || __class.progress[2] != 0 {
                                                Fluent_Rect {
                                                    HStack {
                                                        Text(__class.name).font(.title)
                                                        ProgressView("",value: __class.progress[0]*100 , total: 100)
                                                            .progressViewStyle(LinearProgressViewStyle())
                                                        Text("\(Int(__class.progress[0]*100))%")
                                                            .font(.title)
                                                    }
                                                    .foregroundColor(.white)
                                                    .padding(.all)
                                                }
                                                .frame(height: 100)
                                            }
                                        }
                                    }
                                } //排行榜list
                            } //排行榜
                        }
                        Spacer()
                    }
                }.padding()
            )
    }
}
