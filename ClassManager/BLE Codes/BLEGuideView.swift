//
//  BLEGuideUI.swift
//  DashBoard
//
//  Created by 王麒皓 on 2023/7/15.
//

import SwiftUI

struct BLEGuideView: View {
    @EnvironmentObject var ClassName: ClassNameData
    @State var classNameStr: String
    @Environment(\.presentationMode) var presentation
    @State var isConfigured: Bool = false
    var body: some View {
        for idx in 0..<ClassName.ClassName.count {
            if ClassName.ClassName[idx].name == classNameStr && ClassName.ClassName[idx].BLEName != nil {
                DispatchQueue.main.async {
                    isConfigured = true
                }
                break
            }
        }
        return VStack(spacing: 50) {
            Image(systemName: "wave.3.backward")
                .resizable()
                .scaledToFill()
                .frame(width: 100, height: 100)
                .padding()
            Text("无线打卡向导")
                .font(.title)
            Text("点击继续，将此设备配置为\(classNameStr)班的无线打卡终端\n 配置完成时，app 将重新启动")
                .font(.title2)
            Button(action: {
                //
                print("Clicked!")
//                print(isConfigured)
                // MARK: BLE Setup
                for idx in 0..<ClassName.ClassName.count {
                    if ClassName.ClassName[idx].name == classNameStr {
                        ClassName.ClassName[idx].BLEName = "\(UUID())"
                        break
                    }
                }
                _ = ClassName.uploadDataByHttp()
                UserDefaults.standard.set(true, forKey: "BLE.configured")
                presentation.wrappedValue.dismiss()
                exitApp()
            }) {
                ZStack {
                    RoundedRectangle(cornerRadius: 10)
                        .frame(width: 200, height: 50)
                    Text("立即配置")
                        .foregroundColor(.white)
                }
            }
            .disabled(isConfigured)
            Button(action: {
                // MARK: Unset BLE
                for idx in 0..<ClassName.ClassName.count {
                    if ClassName.ClassName[idx].name == classNameStr {
                        ClassName.ClassName[idx].BLEName = nil
                        break
                    }
                }
                _ = ClassName.uploadDataByHttp()
                UserDefaults.standard.set(false, forKey: "BLE.configured")
                presentation.wrappedValue.dismiss()
                exitApp()
            }) {
                ZStack {
                    RoundedRectangle(cornerRadius: 10)
                        .frame(width: 200, height: 50)
                        .foregroundColor(!isConfigured ? .gray:.red)
                    Text("取消配置")
                        .foregroundColor(.white)
                }
            }
            .disabled(!isConfigured)
            Button(action: {
                //
                for idx in 0..<ClassName.ClassName.count {
                    if ClassName.ClassName[idx].name == classNameStr {
                        print(ClassName.ClassName[idx].BLEName ?? "")
                        print(classNameStr)
                        break
                    }
                }
                presentation.wrappedValue.dismiss()
            }) {
                ZStack {
                    RoundedRectangle(cornerRadius: 10)
                        .frame(width: 200, height: 50)
                        .foregroundColor(.green)
                    Text("退出向导")
                        .foregroundColor(.white)
                }
            }
        }
        .padding()
    }
    
    private func exitApp() {
        DispatchQueue.main.async {
            // 进入后台
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            // 结束应用
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.25) {
              UIApplication.shared.perform(Selector(("terminateWithSuccess")))
            }
        }
    }
}
