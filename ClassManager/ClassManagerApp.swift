//
//  ClassManagerApp.swift
//  ClassManager
//
//  Created by wqh on 1/27/21.
//

import SwiftUI

@main
struct ClassManagerApp: App {
    @State var isNotFirstLaunch = UserDefaults.standard.bool(forKey: "isNotFirstLaunch")
    @State var once = false
    @Environment(\.scenePhase) var scenePhase
    var body: some Scene {
        return WindowGroup {
            if isNotFirstLaunch {
                LoadingView(content: {ContentView(UserData: CoreData(classNameStr: ""), showWarning: .constant(false))})
            }
            else {
                WelcomeView(content: {ContentView(UserData: CoreData(classNameStr: ""), showWarning: .constant(false))})
            }
        }.onChange(of: scenePhase) {
            if $0 == .background {
//                exit(0)
            }
            if $0 == .active && !UserDefaults.standard.bool(forKey: "2023Beta3Fix") && UserDefaults.standard.bool(forKey: "isNotFirstLaunch") && UserDefaults(suiteName: "group.classmanager")!.string(forKey: "server") != nil {
                Patch2023Beta3Fix()
                UserDefaults.standard.set(true, forKey: "2023Beta3Fix")
            }
        }
    }
}
