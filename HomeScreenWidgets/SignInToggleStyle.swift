//
//  SignInToggleStyle.swift
//  HomeScreenWidgetsExtension
//
//  Created by 王麒皓 on 2023/7/21.
//

import Foundation
import SwiftUI
import WidgetKit

struct SignInToggleStyle: ToggleStyle {
    @Environment(\.widgetFamily) var widgetFamily: WidgetFamily
    
    func makeBody(configuration: Configuration) -> some View {
        Capsule()
            .opacity(0.3)
            .foregroundColor(.white)
            .frame(width: widgetFamily == .accessoryRectangular ? 80:120)
            .overlay {
                HStack(alignment: .center) {
                    if !(widgetFamily == .accessoryRectangular && configuration.isOn) {
                        Image(systemName: "wave.3.backward")
                            .foregroundColor(.white)
                            .opacity(1)
                            .minimumScaleFactor(widgetFamily == .accessoryRectangular ? 0.5:1)
                    }
                    Text(configuration.isOn ? "签到中...":(widgetFamily == .accessoryRectangular ? "签到":"立即签到"))
                        .minimumScaleFactor((widgetFamily == .systemSmall || widgetFamily == .accessoryRectangular) ? 0.5:1.0)
                        .foregroundColor(.white)
                        .bold()
                        .opacity(1)
                }
            }
    }
}
