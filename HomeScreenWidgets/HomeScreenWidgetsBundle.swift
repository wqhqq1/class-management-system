//
//  HomeScreenWidgetsBundle.swift
//  HomeScreenWidgets
//
//  Created by 王麒皓 on 2022/12/25.
//

import WidgetKit
import SwiftUI

@main
struct HomeScreenWidgetsBundle: WidgetBundle {
    var body: some Widget {
        HomeScreenWidgets()
        HomeScreenWidgetsLiveActivity()
    }
}
