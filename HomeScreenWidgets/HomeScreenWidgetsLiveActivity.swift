//
//  HomeScreenWidgetsLiveActivity.swift
//  HomeScreenWidgets
//
//  Created by 王麒皓 on 2022/12/25.
//

import ActivityKit
import WidgetKit
import SwiftUI

struct HomeScreenWidgetsAttributes: ActivityAttributes {
    public struct ContentState: Codable, Hashable {
        // Dynamic stateful properties about your activity go here!
        let remainingMinute: ClosedRange<Date>
    }
    
    // Fixed non-changing properties about your activity go here!
    let totalMinute: Int
    let classTypeString: String
    let classString: String
    
}

@available(iOS 16.1, *)
struct HomeScreenWidgetsLiveActivity: Widget {
    var body: some WidgetConfiguration {
        ActivityConfiguration(for: HomeScreenWidgetsAttributes.self) { context in
            // Lock screen/banner UI goes here
            VStack {
                ZStack {
                    LinearGradient(colors: [.blue, .purple, .pink], startPoint: UnitPoint.topLeading, endPoint: UnitPoint.bottomTrailing)
                        .opacity(0.5)
                        .edgesIgnoringSafeArea(.all)
                    HStack(alignment: .center) {
                        VStack(alignment: .leading) {
                            Text("进行中 \(context.attributes.classTypeString)")
                                .fontWeight(.heavy)
                                .foregroundColor(.white)
                                .opacity(0.6)
                            Spacer()
                            Text("上课班级：")
                                .foregroundColor(.white)
                            Text(context.attributes.classString)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                            Text("上课时间：")
                                .foregroundColor(.white)
                            Text(context.state.remainingMinute)
                                .foregroundColor(.white)
                                .fontWeight(.bold)
                        }
                        Spacer()
                        VStack {
                            ProgressView(timerInterval: context.state.remainingMinute, countsDown: true, label: {Text("ABC")}) {
                                VStack {
                                    Text("剩余时间")
                                        .font(.subheadline)
                                    Text(timerInterval: context.state.remainingMinute, countsDown: true)
                                        .font(.headline)
                                }
                            }
                            .progressViewStyle(CircularProgressViewStyle())
                            .tint(.white)
                        }
                        .foregroundColor(.white)
                    }
                    .padding()
                }
            }
            
        } dynamicIsland: { context in
            DynamicIsland {
                // Expanded UI goes here.  Compose the expanded UI through
                // various regions, like leading/trailing/center/bottom
                DynamicIslandExpandedRegion(.leading) {
                    Text(context.attributes.classTypeString)
                        .minimumScaleFactor(0.1)
                        .padding(.leading)
                }
                DynamicIslandExpandedRegion(.trailing) {
                    Text(context.attributes.classString)
                        .minimumScaleFactor(0.1)
                        .padding(.trailing)
                }
                DynamicIslandExpandedRegion(.bottom) {
                    HStack {
                        VStack(alignment: .leading, spacing: 0) {
                            Text("上课时间：")
                            Text("\(context.state.remainingMinute)")
                        }
                        Spacer()
                        VStack {
                            VStack {
                                ProgressView(timerInterval: context.state.remainingMinute, countsDown: true, label: {Text("ABC")}) {
                                    Text(timerInterval: context.state.remainingMinute, countsDown: true)
                                        .minimumScaleFactor(0.1)
                                        .frame(maxWidth: 40)
                                }
                                .progressViewStyle(CircularProgressViewStyle())
                                .tint(.white)
                                .frame(minWidth: 70, minHeight: 70)
                            }
                        }
                        .foregroundColor(.white)
                    }
                    .padding()
                    // more content
                }
            } compactLeading: {
                Text(context.attributes.classTypeString)
            } compactTrailing: {
                Text(timerInterval: context.state.remainingMinute, countsDown: true)
                    .minimumScaleFactor(0.1)
                    .frame(maxWidth: 45)
            } minimal: {
                VStack {
                    Text(context.attributes.classTypeString)
                        .minimumScaleFactor(0.9)
                }
            }
        }
    }
}
